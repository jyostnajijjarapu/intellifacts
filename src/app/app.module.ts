import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormGroupDirective, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { CurrentStatusComponent } from './current-status/current-status.component';
import { MaterialModule } from './materil.module';
import { RunningStatusComponent } from './running-status/running-status.component';
import { MainComponent } from './main/main.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent, CurrentStatusComponent, RunningStatusComponent, MainComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule, FormsModule, ReactiveFormsModule, BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
