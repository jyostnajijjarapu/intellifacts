import { Component, OnInit } from '@angular/core';
import * as ChartDataLabels from 'chartjs-plugin-datalabels';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  data: any;
  plugins: any;
  options: any;
  linesData: any = [
    { img: 'https://primefaces.org/cdn/primeng/images/demo/avatar/asiyajavayant.png', feeling: 'sentiment_neutral', line: 'Line 1', shift: 'B(16:01)', units: '28,080(EACH)', power: '100', tar: '71', tarper: '81', data1: [65, 49, 80, 31, 56, 53, 60], data2: [58, 38, 40, 59, 86, 57, 90], runtar: 71, runper: 80 },
    { img: 'https://primefaces.org/cdn/primeng/images/demo/avatar/asiyajavayant.png', feeling: 'sentiment_neutral', line: 'Line 2', shift: 'B(16:01)', units: '29,406(EACH)', power: '105', tar: '71', tarper: '79', data1: [55, 69, 40, 31, 66, 53, 40], data2: [68, 58, 40, 79, 46, 57, 30], runtar: 74, runper: 34 },
    { img: 'https://primefaces.org/cdn/primeng/images/demo/avatar/asiyajavayant.png', feeling: 'sentiment_dissatisfied', line: 'Packing Line 1', shift: 'B(16:03)', units: '24,718(EACH)', power: '87', tar: '74', tarper: '72', data1: [68, 58, 40, 79, 46, 57, 30], data2: [58, 38, 40, 59, 86, 57, 90], runtar: 74, runper: 69 },
    { img: 'https://primefaces.org/cdn/primeng/images/demo/avatar/asiyajavayant.png', feeling: 'sentiment_satisfied', line: 'Packing Line 2', shift: 'B(16:02)', units: '27,620(EACH)', power: '87', tar: '74', tarper: '80', data1: [65, 49, 80, 31, 56, 53, 60], data2: [68, 58, 40, 79, 46, 57, 30], runtar: 74, runper: 75 },
    { img: 'https://primefaces.org/cdn/primeng/images/demo/avatar/asiyajavayant.png', feeling: 'sentiment_satisfied', line: 'Line 4', shift: 'B(16:02)', units: '25,010(EACH)', power: '84', tar: '74', tarper: '75', data1: [65, 49, 80, 31, 56, 53, 60], data2: [58, 38, 40, 59, 86, 57, 90], runtar: 74, runper: 75 },
    { img: 'https://primefaces.org/cdn/primeng/images/demo/avatar/asiyajavayant.png', feeling: 'sentiment_neutral', line: 'Line 3', shift: 'B(16:01)', units: '26,373(EACH)', power: '87', tar: '72', tarper: '76', data1: [65, 49, 80, 31, 56, 53, 60], data2: [58, 38, 40, 59, 86, 57, 90], runtar: 74, runper: 28 },
    { img: 'https://primefaces.org/cdn/primeng/images/demo/avatar/asiyajavayant.png', feeling: 'sentiment_neutral', line: 'Line 5', shift: 'B(16:01)', units: '27,687(EACH)', power: '87', tar: '74', tarper: '80', data1: [65, 49, 80, 31, 56, 53, 60], data2: [58, 38, 40, 59, 86, 57, 90], runtar: 74, runper: 67 },


  ]
  ngOnInit() {
    this.drawarChart();
  }

  drawarChart() {
    let data1: any;
    let data2: any;
    console.log(this.linesData.length, "len")
    console.log(this.linesData[0]['line'], "data1///////////////////")
    if (this.linesData.length > 0) {
      for (let i = 0; i < this.linesData.length; i++) {
        data1 = this.linesData[i].data1;
        data2 = this.linesData[i].data2;


        const documentStyle = getComputedStyle(document.documentElement);
        const textColor = documentStyle.getPropertyValue('--text-color');
        const textColorSecondary = documentStyle.getPropertyValue('--text-color-secondary');
        const surfaceBorder = documentStyle.getPropertyValue('--surface-border');

        this.data = {
          labels: ['9', '11', '13', '15', '17', '19', '21'],
          datasets: [
            {
              label: 'My First dataset',
              backgroundColor: this.customColors(data1),
              borderColor: this.customColors(data1),
              // backgroundColor: documentStyle.getPropertyValue(color1),
              // borderColor: documentStyle.getPropertyValue('--blue-500'),
              data: data1,

            },

            {

              label: 'My Second dataset',
              backgroundColor: this.customColors(data2),
              
              borderColor: this.customColors(data2),

              // backgroundColor: documentStyle.getPropertyValue(color2),
              // borderColor: documentStyle.getPropertyValue('--pink-500'),
              data: data2,

            }
          ]
        };

        this.linesData[i].data = this.data;


        this.options = {
          responsive: true,
          layout: {
            padding: 20
          },
          // maintainAspectRatio: false,
          aspectRatio: 2.5,
          plugins: {
            legend: {
              display: false,
            },
          },
          scales: {
            x: {
              ticks: {
                color: textColorSecondary,
                font: {
                  weight: 500
                },

              },

              grid: {
                color: surfaceBorder,
                drawBorder: false,
                display: false
              },

            },
            y: {

              ticks: {
                display: false,
                color: textColorSecondary
              },
              grid: {
                color: surfaceBorder,
                drawBorder: false,
                display: false

              },
              display: false
            }

          },

        };
        this.plugins = [this.topLabels]
      }
    }

  }
  topLabels = {
    id: 'topLabels',
    afterDatasetsDraw(chart: any, args: any, pluginOptions: any) {
      const { ctx, scales: { x, y } } = chart

      ctx.font = 'bold 12px sans-serif';
      ctx.fillStyle = 'rgba(255,26,104,1)';
      ctx.textAlign = 'center';

      let base_image = new Image();
      base_image.src = 'assets/images/nobg-star.png';
      base_image.onload = function () {
        // ctx.drawImage(base_image, x.getPixelForValue(0), chart.getDatasetMeta(1).data[0].y, 30, 30);
        for (let index = 0; index < 2; index++) {
          for (let i = 0; i < chart.getDatasetMeta(0)._parsed.length; i++) {
            console.log(chart.getDatasetMeta(0)._parsed[i].y > 75)
            if (chart.getDatasetMeta(index)._parsed[i].y > 55) {

              ctx.drawImage(base_image, chart.getDatasetMeta(index).data[i].x - 10, chart.getDatasetMeta(index).data[i].y - 20, 20, 20);
              // ctx.drawImage(base_image, chart.getDatasetMeta(1).data[1].x - 15, chart.getDatasetMeta(1).data[1].y - 30, 30, 30);
              // ctx.drawImage(base_image, chart.getDatasetMeta(1).data[2].x - 15, chart.getDatasetMeta(1).data[2].y - 30, 30, 30);
              // ctx.drawImage(base_image, chart.getDatasetMeta(1).data[3].x - 15, chart.getDatasetMeta(1).data[3].y - 30, 30, 30);
              // ctx.drawImage(base_image, chart.getDatasetMeta(1).data[4].x - 15, chart.getDatasetMeta(1).data[4].y - 30, 30, 30);
              // ctx.drawImage(base_image, chart.getDatasetMeta(1).data[5].x - 15, chart.getDatasetMeta(1).data[5].y - 30, 30, 30);
              // ctx.drawImage(base_image, chart.getDatasetMeta(1).data[6].x - 15, chart.getDatasetMeta(1).data[6].y - 30, 30, 30);


            }
          }
        }


        console.log(chart.getDatasetMeta(1), "mata")
        console.log(chart.getDatasetMeta(0)._dataset.data, "llllllllll")
        // ctx.drawImage(base_image, chart.getDatasetMeta(0).data[1].x - 15, chart.getDatasetMeta(0).data[1].y - 30, 30, 30);


        ctx.textAlign = 'center';
      }
      // ctx.fillText('19', x.getPixelForValue(0), chart.getDatasetMeta(1).data[0].y - 10)
    }
  }
  customColors(data: any) {
    let customColours = []
    if (data.length) {
      customColours = data.map((element: any, idx: any) => {
        if (element > 50) {
          return '#33BE33'
        }
        else {
          return '#CC3F3D'
        }
      });
    }
    return customColours;
  }
  gotoRunningStatus() {
    window.open('/runningStatus')
  }
}
