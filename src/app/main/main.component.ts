import { Component, OnInit } from '@angular/core';
import { menuItems } from '../sidemenu';
import { MenuItem } from 'primeng/api';
import { Router } from '@angular/router';
import { ConfirmationService, MessageService } from 'primeng/api';
interface City {
  name: string;
  code: string;
}
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  providers: [ConfirmationService, MessageService]
})
export class MainComponent implements OnInit {
  menuOptions: any = JSON.parse(JSON.stringify(menuItems));
  sidebarVisible: boolean = true;
  items: MenuItem[] | undefined;

  home: MenuItem | undefined;
  languages: City[] | undefined;

  selectedCity: City | undefined;
  constructor(private router: Router, private confirmationService: ConfirmationService, private messageService: MessageService) { }
  ngOnInit() {
    // this.items = [
    //   {
    //     label: 'Update',
    //     icon: 'pi pi-refresh'
    //   },
    //   {
    //     label: 'Delete',
    //     icon: 'pi pi-times'
    //   },
    //   {
    //     label: 'Angular',
    //     icon: 'pi pi-external-link',
    //     url: 'http://angular.io'
    //   },
    //   {
    //     label: 'Router',
    //     icon: 'pi pi-upload',
    //     routerLink: '/fileupload'
    //   }
    // ];
    this.items = [{ label: 'Unilever' }];

    this.languages = [
      { name: 'English', code: 'ENG' },

    ];
  }
  logout(event: Event) {
    this.confirmationService.confirm({
      target: event.target as EventTarget,
      message: 'Are you sure, you want to logout?',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.messageService.add({ severity: 'info', summary: 'Confirmed', detail: 'You have accepted' });
        this.router.navigate(['login'])
      },
      reject: () => {
        this.messageService.add({ severity: 'error', summary: 'Rejected', detail: 'You have rejected' });
      }
    });

  }
  applyActiveLinkBasedOnRoute(menuItem: any) {
    return this.router.url.includes(menuItem.routerLink);
  }
}
