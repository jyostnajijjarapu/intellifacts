import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lines',
  templateUrl: './lines.component.html',
  styleUrls: ['./lines.component.scss']
})
export class LinesComponent {
  linesList = ['Akash Machine 1', 'Akash Machine 13', 'Akash Machine 2', 'Bosch 4 ST'];
  constructor(private router: Router) { }
  gotoMachines() {
    this.router.navigate(['main/machines'])
  }
  gotoDashboard() {
    window.open('/home')
  }
}
