import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main.component';
import { FactoriesComponent } from './factories/factories.component';
import { LinesComponent } from './lines/lines.component';
import { MachinesComponent } from './machines/machines.component';

const routes: Routes = [
  {
    path: '', component: MainComponent,
    // {
    // path: 'main', component: MainComponent,
    children: [
      { path: 'factories', component: FactoriesComponent },
      { path: 'lines', component: LinesComponent },
      { path: 'machines', component: MachinesComponent },

    ]
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
