import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { FactoriesComponent } from '../main/factories/factories.component'
import { LinesComponent } from '../main/lines/lines.component';
import { MachinesComponent } from '../main/machines/machines.component';
import { MaterialModule } from '../materil.module';
@NgModule({
  declarations: [
    FactoriesComponent, LinesComponent, MachinesComponent
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    MaterialModule
  ]
})
export class MainModule { }
