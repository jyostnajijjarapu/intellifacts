import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-factories',
  templateUrl: './factories.component.html',
  styleUrls: ['./factories.component.scss']
})
export class FactoriesComponent {
  factoryList = ['Amli-Hair Care', 'Amli-Tea', 'Amli-Hair Care', 'Amli-Tea', 'Amli-Hair Care', 'Amli-Tea', 'Amli-Hair Care', 'Amli-Tea',];
  constructor(private router: Router) { }
  gotoLines() {
    this.router.navigate(['main/lines'])
  }
}
