import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-machines',
  templateUrl: './machines.component.html',
  styleUrls: ['./machines.component.scss']
})
export class MachinesComponent {
  factoryList = ['FG Powder Hopper', 'Akash 1', 'Stiching Machine', 'Barcode/PO Reader & EOL',];
  constructor(private router: Router) { }
  gotoLines() {
    this.router.navigate(['main/lines'])
  }
}
