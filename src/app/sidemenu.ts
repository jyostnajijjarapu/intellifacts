export const menuItems = [
    {
        title: 'Lines',
        routerLink: 'factories',
        icon: 'person',
    },
    {
        title: 'Factory Reports',
        routerLink: 'reports',
        icon: 'location_city',
    },
    {
        title: 'Configurator',
        routerLink: 'configurator',
        icon: 'settings',
    },
]
