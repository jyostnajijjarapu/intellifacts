import { Component } from '@angular/core';
import { plugins } from 'chart.js';

@Component({
  selector: 'app-running-status',
  templateUrl: './running-status.component.html',
  styleUrls: ['./running-status.component.scss']
})
export class RunningStatusComponent {
  value: number = 90.5;
  data: any;
  plugins: any;
  options: any;
  data1: any;
  options1: any;
  donutdata: any = [
    { 'targetValue': 72, 'receivedValue': 90.5 }
  ];
  tableData = [
    {
      prd: 'quality',
      code: '1 X MATERIAL - No Cans',
      total: '23m',
      totaleach: '2,300',
      totalImpact: '115.00',
      background: 100
    },
    {
      prd: 'quality',

      code: '1 X CONVEYORS - Salts Damaged or Broken',
      total: '7m',
      totaleach: '700',
      totalImpact: '35.00',
      background: 30
    },
    {
      prd: 'quality',

      code: '1 X CONVEYORS - Incline Belt Damage',
      total: '5m',
      totaleach: '500',
      totalImpact: '25.00',
      background: 21
    },
    {
      prd: 'performance',

      code: '1 X TRAY PACKER - Case Stuck',
      total: '4m',
      totaleach: '400',
      totalImpact: '20.00',
      background: 17
    },
    {
      prd: 'performance',

      code: '1 X PALLETIZER - Film Break',
      total: '5m',
      totaleach: '337',
      totalImpact: '16.85',
      background: 17
    },
  ]

  targetValue = 72;
  ngOnInit() {
    let chartData = [this.donutdata[0].receivedValue, 100 - this.donutdata[0].receivedValue]
    const documentStyle = getComputedStyle(document.documentElement);
    const textColor = documentStyle.getPropertyValue('--text-color');

    this.data = {
      // labels: ['A', 'B', 'C'],
      datasets: [
        {
          data: chartData,
          backgroundColor: [this.customColors(chartData), documentStyle.getPropertyValue('--gray-500')],
          // borderWidth: 5

          // backgroundColor: [documentStyle.getPropertyValue('--blue-500'), documentStyle.getPropertyValue('--yellow-500'), documentStyle.getPropertyValue('--green-500')],
          // hoverBackgroundColor: [documentStyle.getPropertyValue('--blue-400'), documentStyle.getPropertyValue('--yellow-400'), documentStyle.getPropertyValue('--green-400')]
        }
      ]
    };
    this.data1 = {
      // labels: ['A', 'B', 'C'],
      datasets: [
        {
          data: [72, 5, 23],
          backgroundColor: [
            "rgba(0,0,0,0)",
            "black",
            "rgba(0,0,0,0)",
          ],
          borderColor: [
            'rgba(0, 0, 0 ,0)',
            'white',
            'rgba(0, 0, 0 ,0)'
          ],
          borderRadius: 10,
          // data: ["", 1],
          // backgroundColor: [this.customColors(chartData), documentStyle.getPropertyValue('--gray-500')],
          // borderWidth: 5

          // backgroundColor: [documentStyle.getPropertyValue('--blue-500'), documentStyle.getPropertyValue('--yellow-500'), documentStyle.getPropertyValue('--green-500')],
          // hoverBackgroundColor: [documentStyle.getPropertyValue('--blue-400'), documentStyle.getPropertyValue('--yellow-400'), documentStyle.getPropertyValue('--green-400')]
        }
      ]
    };


    this.options = {
      borderWidth: 1,
      cutout: '85%',
      circumference: 180,
      rotation: 270,
      // cutoutPercentage: 95,

      plugins: {
        legend: {
          labels: {
            color: textColor,
            display: false
          },
          display: false
        },
        tooltip: {
          enabled: false
        },
        // cutoutPercentage: 95,

      },

      // rotation: 1 * Math.PI,
      // circumference: 1 * Math.PI
    };
    this.options1 = {
      borderWidth: 1,
      cutout: '85%',
      circumference: 180,
      rotation: 270,
      // cutoutPercentage: 95,

      plugins: {
        legend: {
          labels: {
            color: textColor,
            display: false
          },
          display: false
        },
        tooltip: {
          enabled: false
        },
        // cutoutPercentage: 95,

      },

      // rotation: 1 * Math.PI,
      // circumference: 1 * Math.PI
    };
    this.plugins = [this.centerText];
  }
  centerText = {
    id: 'centerText',
    afterDatasetsDraw(chart: any, args: any, option: any) {
      const { ctx, chartArea: { left, right, top, bottom, width, height } } = chart;
      ctx.save();
      ctx.font = 'bolder 20px Arial';
      ctx.fillStyle = 'rgb(72,126,72)';
      ctx.textAlign = 'center';
      ctx.fillText('90.5%', 75, 75 + 40)
    }
  }
  customColors(data: any,) {
    let charColor = '';
    if (this.donutdata[0].targetValue != undefined) {

      if (this.donutdata[0].targetValue >= 72) {
        return 'green'
      }
      else {
        return 'red'
      }

    }
    return charColor;
  }



}
