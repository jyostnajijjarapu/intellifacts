import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { PasswordModule } from 'primeng/password';
import { AvatarModule } from 'primeng/avatar';
import { DropdownModule } from 'primeng/dropdown';
import { DividerModule } from 'primeng/divider';
import { ProgressBarModule } from 'primeng/progressbar';
import { ChartModule } from 'primeng/chart';
import { KnobModule } from 'primeng/knob';
import { SidebarModule } from 'primeng/sidebar';
import { ToolbarModule } from 'primeng/toolbar';
import { SplitButtonModule } from 'primeng/splitbutton';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { ConfirmPopupModule } from 'primeng/confirmpopup';
import { ToastModule } from 'primeng/toast';
import { TableModule } from 'primeng/table';

const materialModules = [
    CardModule,
    ButtonModule, InputTextModule, PasswordModule, AvatarModule, TableModule,
    DropdownModule, DividerModule, ProgressBarModule, ChartModule,
    KnobModule, SidebarModule, ToolbarModule, SplitButtonModule, BreadcrumbModule, ConfirmPopupModule, ToastModule
]
@NgModule({
    imports: [CommonModule, ...materialModules],
    exports: [...materialModules]
})
export class MaterialModule { }